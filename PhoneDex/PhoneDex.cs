﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneDex
{
    public class PhoneDex
    {
        public string Alias { get; set; }
        public string PhoneNumber { get; set; }

        public PhoneDex(string alias, string phonenumber)
        {
            Alias = alias;
            PhoneNumber = phonenumber;
        }

        public static Dictionary<string, PhoneDex> SetNumbers()
        {
            var numbers = new Dictionary<string, PhoneDex>();
            Console.WriteLine("Please enter a Phone Number:");
            var theNumber = new PhoneDex("", Console.ReadLine());
            Console.WriteLine("Please Enter an Alias:");
            numbers.Add(Console.ReadLine(), theNumber);
            return numbers;
        }
    }
}
